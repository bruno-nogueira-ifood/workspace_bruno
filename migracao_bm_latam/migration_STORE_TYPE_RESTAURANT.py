import os
import sys
import logging
import requests #To see more about requests lib http://docs.python-requests.org/en/master/user/quickstart/

logging.basicConfig(format='%(asctime)s %(message)s', level=logging.DEBUG)

env = sys.argv[1] if len(sys.argv) > 1 else 'DEV'

merchant_url=None

def get_stores_id_without_group(limit):
    query = """
        select st.id
        from ifood_merchant.merchant_stores st
        where not exists (
            select 1 from ifood_merchant.store_configs configs
            where configs.store_id = st.id and configs."key" = 'LOAD_TEST' and configs.value = 'S'
            ) and not exists (
                select 1 from ifood_merchant.store_configs configs
                where configs.store_id = st.id and configs."key" = 'TESTE' and configs.value = 'S'
            )
        and not exists (
                select 1 from ifood_merchant.rel_group_store rg
                        inner join ifood_merchant."group" g on g.id = rg.group_id
                where rg.store_id = st.id and g."type" = 'STORE_TYPE'
            )
        limit %d;
    """ % (limit)    
    select = 'psql -AXtq -d "service=merchant-ro" -c "' + query + '"'
    rows = os.popen(select).read()
    rows = [] if len(rows) <= 1 else rows
    if (len(rows) > 0):
        rows = rows.strip().split("\n")
    return rows


def get_group_uuid(group_external_id):
    url = merchant_url + '/group/BUSINESS_MODEL/' + group_external_id + '/id'
    result = requests.get(url)
    return result.json()


def add_store_to_group(store_id, group_id):
    url = merchant_url + '/group/' + group_id + '/store/' + store_id
    headers = dict()
    headers['system-id'] = 'script-bruno'
    headers['user-id'] = 'bruno.nogueira'
    requests.put(url, headers=headers)
    


def run():
    # get desired group uuid
    group_id = get_group_uuid('RESTAURANT')
    
    # get stores to migrate
    stores_ids = get_stores_id_without_group(4000)

    # iterate on stores changing business model
    for store_id in stores_ids:
        add_store_to_group(store_id, group_id)
    



if __name__ == '__main__':
    if env == 'DEV':
        merchant_url = 'http://sa-east-1-elb-ifood-service-merchant.aws-development.dc-ifood.com/merchant'
        run()

    elif env == 'PROD':
        merchant_url = 'http://us-east-1-elb-ifood-service-merchant.aws-prod-legacy.dc-ifood.com/merchant'
        run()
    else:
        logging.info("You have to choose the right env: PROD or DEV")