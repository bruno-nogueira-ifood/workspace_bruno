# -*- coding: utf-8 -*-

import os
import sys
import logging
import requests #To see more about requests lib http://docs.python-requests.org/en/master/user/quickstart/

logging.basicConfig(format='%(asctime)s %(message)s', level=logging.DEBUG)

merchant_url=None

env = sys.argv[1] if len(sys.argv) > 1 else 'DEV'


def create_config(rest_uuid):
    config = dict()
    config["ids"] = rest_uuid
    config["key"] = config_name
    config["value"] = 'S'

    logging.debug(config)
    requests.post(merchant_url + '/store/config/batch/', json=config)



def run():
    


if __name__ == '__main__':
    if env == 'DEV':
        merchant_url = 'http://sa-east-1-elb-ifood-service-merchant.aws-development.dc-ifood.com/merchant'
        run()

    elif env == 'PROD':
        merchant_url = 'http://sa-east-1-elb-ifood-service-merchant.aws-production.dc-ifood.com/merchant'
        run()
    else:
        logging.info("You have to choose the right env: PROD or DEV")