# -*- coding: utf-8 -*-

import os
import sys
import logging
import requests #To see more about requests lib http://docs.python-requests.org/en/master/user/quickstart/

logging.basicConfig(format='%(asctime)s %(message)s', level=logging.DEBUG)

merchant_api_url=None
product_external_id = 'takeout'
status = 'ACQUIRED'


env = sys.argv[1] if len(sys.argv) > 1 else 'DEV'

def get_restaurant_id_by_product_id_and_status(external_id, st, page):
    query = """
        select store_id
        from rel_merchant_product rmp
                inner join product p on rmp.product_id = p.id
        where rmp.status = '%s'
            and p.external_id = '%s'
        order by store_id
        limit %d
        offset %d;
    """ % (st, external_id, 100, 100 * page)
    select = 'psql -AXtq -d "service=merchant-products-ro" -c "' + query + '"'
    rows = os.popen(select).read()
    rows = [] if len(rows) <= 1 else rows
    if (len(rows) > 0):
        rows = rows.strip().split("\n")
    return rows

def get_TO_GO_SETTING(store_id):
    logging.debug("Getting TO_GO_SETTING of merchant: " + store_id)
    url = merchant_api_url + '/store/config/%s/%s/' % (store_id, 'TO_GO_SETTING')
    result = requests.get(url)
    return result.json()

def delete_TO_GO_SETTING(store_id):
    logging.debug("Removing TO_GO_SETTING from Merchant: " + store_id)
    file = open("REMOVED_TO_GO_SETTINGS.csv", "a")
    content = "%s;takeout;ACQUIRED;S\n" % (store_id)
    file.write(content)
    file.close()
    # url = merchant_api_url + '/store/config/%s/%s/' % (store_id, 'TO_GO_SETTING')
    # requests.delete(url)

def get_TO_GO_ENABLED(store_id):
    logging.debug("Getting TO_GO_ENABLED of merchant: " + store_id)
    url = merchant_api_url + '/store/config/%s/%s/' % (store_id, 'TO_GO_ENABLED')
    result = requests.get(url)
    return result.json()

def add_inconsistent_value_on_file(store_id):
    logging.debug("Adding Store Id on inconsistent status")
    file = open("inconsistente_ACQUIRED_TO_GO_ENABLED.csv", "a")
    content = "%s;takeout;ACQUIRED;N\n" % (store_id)
    file.write(content)
    file.close()


def validate_TO_GO_config(store_id):
    result = get_TO_GO_SETTING(store_id)
    logging.debug("Store id: " + result["storeId"] + " config: " + result["key"])
    if ("value" in result and result["value"] == 'S'):
        delete_TO_GO_SETTING(store_id)
    result = get_TO_GO_ENABLED(store_id)
    if ("value" in result and result["value"] == 'N'):
        add_inconsistent_value_on_file(store_id)


def run():
    page = 0
    stores_ids = get_restaurant_id_by_product_id_and_status(product_external_id, status, page)
    while(len(stores_ids) >= 1):
        for store_id in stores_ids:
            validate_TO_GO_config(store_id)
        page = page + 1
        stores_ids = get_restaurant_id_by_product_id_and_status(product_external_id, status, page)



    # lista os restaurantes no Merchant Products cujo status esteja em ACQUIRED
    # para cada restaurante, 
    #   - busca no Merchant se tem a config TO_GO_SETTING = 'N'. 
    #       - Caso positivo, remove a config do Merchant para o restaurante
    #   - busca no Merchant se tem a config TO_GO_ENABLED = 'S'
    #       - Caso negativo, escreve no arquivo inconsistente_ACQUIRED_TO_GO_ENABLED.csv

    # lista os restaurantes no Merchant Products cujo status esteja em REMOVED
    # para cada restaurante, 
    #   - busca no Merchant se tem a config TO_GO_SETTING = 'S'. 
    #       - Caso positivo, remove a config do Merchant para o restaurante
    #   - busca no Merchant se tem a config TO_GO_ENABLED = 'N'
    #       - Caso negativo, escreve no arquivo inconsistente_REMOVED_TO_GO_ENABLED.csv



    # lista os restaurantes no Merchant Products cujo status esteja em REMOVED
    # para cada restaurante, busca no Merchant se tem a config TO_GO_ENABLED = 'S'. 
    #   - Caso negativo, escreve no arquivo inconsistente_ACQUIRED_TO_GO_ENABLED.csv
    

if __name__ == '__main__':
    if env == 'DEV':
        merchant_api_url = 'http://sa-east-1-elb-ifood-service-merchant.aws-development.dc-ifood.com/merchant'
        run()

    elif env == 'PROD':
        merchant_api_url = 'http://us-east-1-elb-ifood-service-merchant.aws-prod-legacy.dc-ifood.com/merchant'
        run()
    else:
        logging.info("You have to choose e right env: PROD or DEV")