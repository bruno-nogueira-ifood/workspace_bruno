# -*- coding: utf-8 -*-

import os
import sys
import logging
import requests #To see more about requests lib http://docs.python-requests.org/en/master/user/quickstart/

# Resync de Store Config of all restaurants on application

logging.basicConfig(format='%(asctime)s %(message)s', level=logging.DEBUG)

merchant_products_api_url='http://ifood-service-merchant-products.aws.nv-restaurant.k8s.aws-production.dc-ifood.com/merchant-products'


def retrieve_products():
    query = """
        select rmp.store_id, p.external_id
        from rel_merchant_product rmp
                inner join product p on rmp.product_id = p.id
        where status IN ('INTEGRATION_FAILED')
            and requisition_date < now() - interval '1 hour'
        order by requisition_date desc
    """
    select = 'psql -AXtq -d "service=merchant-products-ro" -c "' + query + '"'
    rows = os.popen(select).read()
    rows = [] if len(rows) <= 1 else rows
    if (len(rows) > 0):
        rows = rows.strip().split("\n")
    return rows

def reprocess(store_id, product_external_id):
    url = merchant_products_api_url + '/merchants/' + store_id + '/products/' + product_external_id + ':reprocess'
    requests.put(url)


def run():
    logging.debug("Starting process")
    results = retrieve_products()
    for result in results:
        register = result.split("|")
        # logging.debug("store_id " + register[0])
        # logging.debug("product external id " + register[1])
        reprocess(register[0], register[1])
        

    
run()