from datetime import datetime
import uuid
import os


def insert_data_on_database(query):
    select = 'psql -AXtq -d "service=merchant-products-rw" -c "' + query + '"'
    rows = os.popen(select).read()


def run():
    file = open("mkt-merchants.csv", "r")
    sql = open("inserts.sql", "w")

    # ler a primeira linha do arquivo
    merchant_id = file.readline().replace('\n', '').replace('\"', '')
    # inicia o contador
    counter = 0

    #cria a base das queries
    rel_mp_sql = " insert into rel_merchant_product (store_id, product_id, status, user_email, requisition_date, system_id) values "
    audit_sql = " insert into product_acquisition_audit (id, store_id, product_id, product_external_id, user_email, requisition_date, requisition_status, system_id) VALUES "

    # enquanto o contador for menor do que sem e existir um merchant_id    
    while merchant_id:

        #2020-06-04 13:39:37.017231
        formated_date = datetime.now().strftime('%Y-%m-%d %H:%M:%S.000')

        # adiciona o statement relacionado aquele merchant id no rel_mp_sql e audit_sql
        rel_statment = ""
        audit_statment = ""
        if counter != 0:
            rel_statment = ','
            audit_statment = ','
        rel_statment = rel_statment + "('%s', '%s', '%s', '%s', '%s', '%s')" % (merchant_id, '8b3eb533-f349-473d-bd29-a2445e999fd8', 'ACQUIRED', 'bruno.nogueira@ifood.com.br', formated_date, 'MANUAL_SCRIPT')
        audit_statment = audit_statment + "('%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s')" % (uuid.uuid4(), merchant_id, '8b3eb533-f349-473d-bd29-a2445e999fd8', 'marketplace', 'bruno.nogueira@ifood.com.br', formated_date, 'ACQUIRED', 'MANUAL_SCRIPT') 
        rel_mp_sql = rel_mp_sql + rel_statment
        audit_sql = audit_sql + audit_statment

        # incrementa o contador e pega o proximo merchant id
        counter = counter + 1
        merchant_id = file.readline().replace('\n', '').replace('\"', '')

        # se o contador bateu 100 ou nao houver mais merchant_id
        if counter == 100 or not merchant_id:
            rel_mp_sql = rel_mp_sql + ";\r\n"
            # fecha a query e executa
            print(rel_mp_sql)
            sql.write(rel_mp_sql)
            insert_data_on_database(rel_mp_sql)

            audit_sql = audit_sql + ";\r\n"
            print(audit_sql)
            sql.write(audit_sql)
            insert_data_on_database(audit_sql)

            # reinicia as queries bases
            rel_mp_sql = " insert into rel_merchant_product (store_id, product_id, status, user_email, requisition_date, system_id) values "
            audit_sql = " insert into product_acquisition_audit (id, store_id, product_id, product_external_id, user_email, requisition_date, requisition_status, system_id) VALUES "

            # reinicia o contador
            counter = 0

    file.close()
    sql.close()

        




run()