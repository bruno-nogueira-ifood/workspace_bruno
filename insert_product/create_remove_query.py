import uuid
import os

def get_product(merchant_id):
    query = """
        select p.id, p.external_id from rel_merchant_product rmp
        inner join product p on rmp.product_id = p.id and p.external_id in ('full-service', 'flex-logistics')
        where rmp.store_id = '%s';
    """ % (merchant_id)
    select = 'psql -AXtq -d "service=merchant-products-rw" -c "' + query + '"'
    rows = os.popen(select).read()
    return rows
    

def run():
    file = open('remove.csv', 'r')
    merchant_id = file.readline().replace('\n', '').replace('\"', '')

    audit_sql = "insert into product_acquisition_audit (id, store_id, product_id, product_external_id, user_email, requisition_date, requisition_status, system_id) VALUES "
    audit_statment = ""
    count = 0

    while merchant_id:
        product = get_product(merchant_id)
        product_id = product.split('|')[0].replace('\n', '')
        product_external_id = product.split('|')[1].replace('\n', '')
    
        audit_statment = audit_statment + "('%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s')" % (uuid.uuid4(), merchant_id, product_id, product_external_id, 'bruno.nogueira@ifood.com.br', '2020-10-20 15:00:17.557589', 'REMOVED', 'MANUAL_SCRIPT') 

        merchant_id = file.readline().replace('\n', '').replace('\"', '')
        count = count+1
        
    audit_sql = audit_sql + audit_statment
    
    result = open('result.sql', 'w')
    result.write(audit_sql)
    print('Quantidade de registros gerados: ' + str(count))
    

    result.close()
    file.close()


run()