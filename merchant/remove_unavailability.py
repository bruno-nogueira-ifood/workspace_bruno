# -*- coding: utf-8 -*-

import os
import sys
import logging
import requests #To see more about requests lib http://docs.python-requests.org/en/master/user/quickstart/

# Resync de Store Config of all restaurants on application

logging.basicConfig(format='%(asctime)s %(message)s', level=logging.DEBUG)

merchant_api_url='http://us-east-1-elb-ifood-service-merchant.aws-prod-legacy.dc-ifood.com/merchant'

def retrieve_ids():
    query = """
    select merchant_id, id
    from ifood_merchant.merchant_unavailabilities
    where active
    and merchant_id in ('a495abc2-4f54-430a-8ae5-8c6aa22d306a', '947342e2-9282-490a-973a-4b90d3922dcb',
                        '7460cc3e-f64a-4ca9-aedf-0b25fdf92ef8', '4b90132a-1614-431c-a38f-976b821ab176',
                        '573ffae9-c61e-4da2-ae49-51359636a0a2', '6c1f1c99-68bc-49cd-b983-f664efe0f6db',
                        '35afbf9f-3fcf-4cfc-94a8-20c2ecc0042c', '3fa37467-7d05-42ad-af99-80c912674184',
                        '8ade5715-252d-4b27-969d-2a9c3b3fca9c', '3b76c5f6-ed3e-41b8-9270-a3fa6e739cae',
                        'a485d2bc-94bd-45ca-bd65-63dfd55dcf7a', 'd4382ac8-ca7b-4ea5-b6b7-adb8928a7bdc',
                        'c3f798ed-e37c-4f1b-a0f4-5fb4f68f7653', '33283074-37fa-4c2e-9c94-120af133fd16',
                        '5018c9f1-7a66-4f8a-ab3d-14fc809a73a8', '91a9dfb3-d470-446a-930c-bfa99685ca74',
                        '7aca7a68-02e6-4ad7-b49e-5311c70b0066', 'dde2f8be-fbc7-4b14-8e5b-e4072d13db83',
                        'f6da3683-431f-4b60-8634-6dc2149e508d', '671ab44c-7eb5-4d6d-80c2-31eba3099938',
                        'fe072eb9-8d14-4dc0-bff4-d164c2c3d91b', '72b5eb3f-62df-432b-a636-27211e08fb59',
                        'cfa08fd6-f079-4ef5-a49f-4d2e8afe3f79', '1db304c2-0a8e-4a69-a1fb-9024d64f348a',
                        '4483f0bd-1dc6-485c-af0a-64762decafd6', '3bfd07a4-c290-49b5-9684-00c2c240bb20',
                        '68ffbcdc-7182-4661-96fa-bcb7cc893b0e', 'd7842798-3bf7-4632-970c-3f63b36bec95',
                        'a14317eb-350b-4e4f-a77b-a7a7da6155ec', '8f12a2be-cf6c-441b-a03a-f8657e3e9e24',
                        '2f8a491b-7dd8-4a19-a7f8-f5da1f0c8f7f', 'ff67b409-35b8-4ffd-9bca-021f1c19ca06',
                        '39d22fe3-b4a9-4671-becd-27685f8f65a5', '4eafa2c6-d53e-4757-9e60-371bd4b858e1',
                        '2b36588b-431d-4d00-a7a5-670481c1897e', 'bede085e-ebc9-4682-8599-0255f0493b5d',
                        '2a698199-49a9-464f-9256-cdce6a0931e6', '9971ffd0-8504-4a41-9537-30ad00602eae',
                        '2fe85686-c1da-4cd5-b736-e7bd5c15cfa0', '86a440c9-55b8-4c1c-ac0f-7eeb529805ee',
                        'd8b01e5b-8ce2-437f-aacc-89f998d14656', 'b3a1addf-63c1-4105-95a1-ee770a95c0ce',
                        '77fdc47b-688b-46eb-aff4-621157a8d902', '5e4e902f-f56a-4b2a-b6e8-3175eb2efd37',
                        '5d407d21-c7c4-4c47-9cad-a9eb394985c8', '474ea56f-768e-4846-8029-a09df5e6ff96',
                        'b2109b9d-a767-4011-b364-f013e091973b', '537fd004-dcc6-496c-ad7a-f01fc228edf4',
                        '8dcd4556-7275-4238-a013-cbd736b7f44a', 'a844030b-dea4-4d60-8aab-a564e7dfdfe7',
                        '6f316781-e862-4af5-8289-6dfe0d6097f8', 'f3d5ca99-4d56-4345-9c50-92b1eb946bc9',
                        '207882c5-3c02-4ba4-bf58-cc1e52a04f9e', '16f06615-47e0-4b7f-9eb8-40d91ae5b3df',
                        '6bdc8af4-9aa4-45a5-ac61-6dbc365f64e7', 'c9466d69-b26b-4d90-981f-0795eabe1a6b',
                        '66669786-ae38-44ad-be9f-41253645b82f', 'd58b3b37-f15b-4b98-9e68-ac0a3ab33121',
                        '4062722f-17c0-4cad-9318-87bb8dc72fdf', '3bcd81bb-3576-440b-bab8-917566201790',
                        'd80153fa-76c1-4567-94fa-1856349631a3', 'c5dda635-f175-461b-9688-79e887742ddd',
                        'ba1a937b-27c3-41b9-8c08-7ca400dc63e4', 'f3b4b758-ad63-4a7f-9132-9fd040c7ffd8',
                        '229df524-3c00-468a-af69-62bb52afbaf4', '389375ce-9853-4ece-8e7d-4dbb531575a0',
                        '507181b9-ba8c-4536-b790-928dfc18cd24', '8c653257-85ec-4ea0-8a39-12b84c0a14d6',
                        '70f810f6-9929-4645-af7f-39f7c33d6012', '4021bd52-c1e3-44bd-b746-09c647597706',
                        'bff4b876-e9a5-4fd1-80ad-1ee5ae13938e', '2f563639-d675-4781-8f36-eb8fc2c7565c',
                        '19d64906-138f-46f8-9a4a-e3a53c855e74', 'cf0833d9-ce9e-487f-b3ac-3e568a08f3c3',
                        '043666ff-a1ce-4723-b14a-d812b9e3c5ff', '879a9f53-68e9-4269-b1e7-f01538baac1c',
                        '913b4df0-b0e0-4291-ad76-c05fb5f0e3fd', '282b584f-4962-4d61-9829-acf53e621db5',
                        '067471a5-1f1f-4b17-b03f-003227f392f5', '51afb44a-ea23-4252-ae4c-367e414e53ee',
                        '69241cd4-2c12-47a0-9c35-4d5f8ebca047', '8c8c3b66-0798-404c-9f45-f7cbc175f3de',
                        '7ed523d0-fb9d-4f3a-b97b-27791d100740', 'fd8ceb57-74d7-4ad2-9e91-0994feebbe6a',
                        'ea21c9e3-e584-46cd-840f-2367b147126b', 'cb1789fd-a859-4952-856c-a6332ce7c568',
                        'bcb01432-1fc0-47f0-b0f4-ee8fbcbf8905', '74ed657c-ff28-4357-b6b2-30c53390b5db',
                        'd33a9ecd-8c00-4122-bc57-a4265406c6b1', '7d00a462-425c-416a-8d75-958d2cc52a21',
                        '62df3733-e5fb-4686-9b5a-cd11b0709fd4', 'afbf0ba0-1f5a-47f2-a3d6-f7cbcf200a66',
                        '4f745a42-1c78-4153-867a-a9c259f35719', '6408e7f3-4de6-4bbf-8071-ae9576acc8b8',
                        '3fc079aa-87f2-48ca-9095-07371016236c', '2ec21b0d-d784-40db-848d-b2b159471b01',
                        'e08e34e1-5fa0-4c94-a911-755b1a228d0c', '3a11261f-c4fc-4aa6-a149-5c7bcab70277',
                        'e28dd736-aa7e-438b-be05-123e27b621bd', '7608196a-d08d-4649-9ce2-e43fe3a3dec7',
                        'ab65251c-1b45-4342-8dec-6686d24b8092', '94754014-b19b-46cb-a52e-50e651170c7d',
                        'd15bda1f-895f-46aa-9441-361b04afa60b', '3eeb8643-07d7-4898-8cae-3d99ee54d7e9',
                        '761ee37c-33d4-41e7-b77d-a414dc10d401', 'a7029b29-7d40-4b69-b120-b94e1c366756',
                        'e8b146b3-bcfd-4752-a64d-57a8bd208dd6', '8102303b-8432-4f93-ab93-73297c59dc2c',
                        '8fdae076-53d1-46ab-ba74-18d0bba61870', '5f47f639-b17a-42a8-bf0a-13bcff1fc0e9',
                        'c91548c3-d0e6-4caf-b100-cce98425d718', 'bedd53e7-7dd6-4f7f-b9ef-0869c4fa245b',
                        'aa5f4202-02f9-419f-b295-5c3c369db03d', '6f9edc7e-b983-475b-8037-005825ba5547',
                        '1214044c-2475-450e-89b6-0de41b3b2647', '07b175b9-7dc7-431d-9e96-f9139f32067c',
                        '5899710b-7969-44e0-a56d-af9dcf4426c0', '7f6ad2c7-21e8-48d5-a3c5-09b6a9b9352f',
                        'c39144b5-a95e-4361-bd26-3466c112a4fc', '8493c309-694d-4294-b6b8-d6480848e967',
                        'd933e279-024c-4b7e-a939-420ed8f5732e', '8c301c21-1e05-4f00-858f-1ae1d207beae',
                        '95b5f997-8fb3-4fb1-84dc-761c69e0eac3', '891dc181-48b0-4c8d-ba84-0ca7d0d0b4f3',
                        '122de1c6-b543-4005-a4f4-e85decc392d7', '53aa4ea9-523d-4ca7-a901-29e176df91bd',
                        '60198ce5-c687-44b3-94f0-f4287325f0b0', '986c5622-cbee-4047-9227-4b0ae6e27508',
                        'bb4aa1ec-2c93-4817-91c1-9917f08b2845', '462ff5b0-f6f5-49d0-af1d-d3f1b2dd6a06',
                        '13198483-5fbc-422f-8285-69a95e5f406c', 'ca2c31ca-fa99-435d-9221-4eef4a5f159f',
                        'a7ee6241-14d6-4ab9-9ada-a5e0266b4582', 'fff4acbb-4805-4694-a23a-df92180587ff',
                        '96d0032e-eae0-4cee-814d-51c75d8d56e3', '71dfdb92-74e9-4305-8828-f4d4085f00b9',
                        'd3643e71-e3cb-4096-8043-0fe5d4081742', '9068f3dd-0bfe-4d2d-9d0e-5f36f9969ec3',
                        'e01d192d-b8ec-461e-9393-76aaf554f4e9', '1b42f128-f698-4713-ad07-c006159fb703',
                        'a8f506b1-92d2-4213-92f4-9c47ed05a5d3', 'ec1e2eed-078c-401a-abfd-b14e34db518e',
                        'aa178098-32f8-454d-b6b8-1b3a2903b62a', 'd609fe20-687f-4ac2-9a24-5e502a4cb4e4',
                        '6a45475a-ce61-44a8-b9e7-cf67e05066dc', 'b32f1b84-9dbb-4985-8f49-9b488ca527a8',
                        '3d7e83d5-7cd5-4d81-aced-31372cd76d23', 'f679f191-9ea8-4db5-a4e6-41f212c5b0e6',
                        '2445767e-e1cc-4dbf-ab73-730a0c2000c9', '630cd42d-35c4-4e20-8c4a-bb229d7f734c',
                        '3f9cf851-4824-4778-a6c8-e5a0fd29ddb7', '2ffa04af-4c76-4c53-b906-c399867b9853',
                        '80f079b4-4759-43f1-a09f-be66e5e87726', '25c1d0ea-527b-4de4-81b4-879385b1b878',
                        'cfd4439d-9eda-43ad-b7ed-847313b7d616', '5fdfab96-cafb-48e0-8480-927672f1e1e1',
                        '322ccdf6-abeb-4249-9d7b-a0ddb19dbc3b', 'af5125a4-06dd-4743-a5ad-171f543246d4',
                        'ec695bcf-ad7a-4f34-a21b-35c3c7b3968b', 'daada95c-1401-42ec-8765-3f28e3d95a25',
                        '64b70d3c-5927-4a9c-9555-782579382c84', '5ee28635-7ec7-4317-9a1d-0e4b4d4a968f',
                        '5484b558-72a2-4f11-8bda-d3f539db3355', '5f3da74b-12a2-4400-abb5-a7dd6547e859',
                        '65d85f74-20e6-40ac-9a1b-8f93841416f0', 'b4bb9425-7564-4ba3-9b20-745479465577',
                        'c419a46f-db40-4299-9341-56617c1539a0', 'cd2785dd-9a34-4bb8-998a-b032f23d472e',
                        '75c0b2c4-0947-428b-8a34-7639f5d19cf3', '4f2f8c39-a1a9-4b1f-863b-940e66ff3893',
                        '4c358c2d-dcb8-48c2-9e8c-f74739521744', '98649aba-d442-4d0f-831b-8cac1d3c37e0',
                        '7b53e011-f270-4734-b057-cdb96d75bb2f', '72f652c2-2016-4514-a611-a5aaecd39ff9',
                        '1985ff07-31fc-463d-8d5a-159066b5d562', '94fe4bf8-4326-4936-87db-2cda9e43d702',
                        '5b2c5581-041d-4eb2-be7d-4a1dcc7899a7', '10695ae4-326f-4b74-a91c-d788e2c52617',
                        'c3d4a115-e4e1-4cd4-b21d-8c46b37aa5bc', '17c7fb7e-bff8-4268-865c-33ad73d4e6cf',
                        'e08a7979-0270-4223-a06c-3b1283c83b0c', 'c593042e-050f-4b2f-bbdf-bcaa7ecdd8ba',
                        '3f0c971e-38c5-4feb-8e8f-71b22f3b7799', '7270ce8b-9361-4705-ac9d-4e54ad649842',
                        '45e48912-bfbd-4319-9cc0-2049b247c1a5', '2b8f1b32-86b9-436e-af6d-01ed24ce10cb',
                        'a1eed99a-f785-4fcc-b95d-a5ba9929956d', '79d89a72-8d1d-4a31-911e-2a3a7f0e9af2',
                        '670c53f9-ec4d-4176-9c06-af1392c7d172', '8febc29b-21f5-46ff-95c4-be2a68429edb',
                        '2c009f36-08c7-433d-a51a-29141aea61a3', '8b00ab42-54a8-42b7-b47e-7de32db653fc',
                        'e9e2a3b6-85d8-4ee6-95fa-697c7b1f2fd1', '0f507fdd-6093-42c0-a4cb-59f7f6851705',
                        'b3e531ca-e1ff-4767-9868-edde3d624366', '4fb45c04-0ffa-4a2a-9f4f-9eda441b0992',
                        '7d66b961-952a-44d3-95b8-bf689588f8de', 'ecefa876-0769-4155-9d0e-12e493d88b05',
                        '1f83771c-791a-4919-b919-b95b5b74eb79', '9d692173-175b-4ee3-af22-5a185c6a4efe',
                        '7e78ff7e-86ad-4a2b-9781-939e80e99491', 'cf482aab-b96c-4f36-bb26-a23352ddfa3b',
                        'eea361a7-a37c-4fff-be83-33508fcf715f', '8a80988c-1a20-4380-8e83-2c4e07f89a5c',
                        'cbfc7c3a-0075-470e-8d68-4ce400bd37e6', '298bef36-77f6-467a-a70f-4d2f2340b711',
                        'c4886c34-07cc-4a90-b2e5-e1b2803e143d', '12b91ab9-2730-4b5c-abd8-890cb4a48fbf',
                        '60ac42b0-2995-4eb4-906e-733a2c0d6517', 'fc737a46-0633-4d5f-8755-edc5810d1478',
                        '672905e7-b9f7-4abf-aa29-a040e72a1efb', '7b2d5880-c798-499c-a180-b6a4a9b645dc',
                        'a7b89f76-7844-4487-bf18-c7ffdc22007d', 'cdfa8d9a-c571-48d6-a308-d9acbcfc9e36',
                        'cc67e265-8e02-4a4d-ad37-a896152c6f9a', '99213619-72db-4f65-8898-7a5135f2b406',
                        'c55816e0-c667-4150-8017-10560de0678d', '30620401-f5bc-448b-b09f-020fbd597887',
                        '13589651-a154-4338-9607-5780bd4dd118', 'c0b4890d-f6bb-4eee-be03-f2f399b13cfe',
                        'e9013762-807f-4f91-a853-8129a663b3e3', '6abc7bd0-8549-4a70-9acb-d318c0150d1f',
                        '851c0e4d-a372-4ab3-9abe-49be1598fbdb', '7ffe261b-ea01-43ca-b6a5-9d62680a7225',
                        '808f0a5c-5227-43a6-9d61-8b5663370b09', 'cd90b5c1-7e95-4a11-92de-64e76c34a44a',
                        '49307fec-8c79-43ec-9f3f-db76347cdb07', '0501470b-d1f0-4a00-8876-7a233de1bb58',
                        'b0d19537-fcf0-4c0c-bfe0-5d491389a147', '027e3a4e-71b6-4d1d-b375-9259acf0856d',
                        'feee3ad5-7367-41be-9f07-0b6d705ab2ef', '995477db-4f91-40b8-a34e-420298b7bd96',
                        'dd9a7a95-828b-405f-9770-05d7f6f57536', 'c0f8c2bd-5a12-4a74-a2ee-498cbe15f86f',
                        'ef4ae9e9-8f77-44a5-8c1c-94c008420ead', '59d2c4db-7b5c-4e8c-a6e4-6ccc1992a55b',
                        '061f50ea-a2ca-475d-8659-17ad6280ee69', 'eedebf35-05b6-4dc1-a1f6-c9d85c84a403',
                        '262db7ce-55aa-4b6b-8947-9357f13954e6', '5e78e2d6-5485-4594-9079-82213d4d51b7',
                        'e76b01ae-96c6-472b-96fd-2dd8d93d47d4', '401ace25-d685-496a-932c-54dfa36ee2ca',
                        '2f3cf3a2-3140-4d79-a2a2-b1fc8444a19c', 'dc6549ad-d004-467e-8bfa-a4d47b066624',
                        'd5ce2604-e7b5-4f1a-8326-91795c4c539f', 'd5007b10-ee27-4ff2-af09-b38276ffc073',
                        '28c00751-f57e-4168-9b65-0f42b4027a2f', '795740c1-dff9-4490-acf5-7d55d439e2a8',
                        '1ccb0e69-02fc-4d5e-9274-3ab23fcafd4e', 'df35e79e-dd9f-4fff-b22f-c16811255574',
                        'f97e9c15-4efd-44ad-8e87-21434e0f25b2', '8bda01f1-5706-42cb-8fe5-1291248a4c4f',
                        '244fa378-01e8-427d-a288-e1032c83216f', '09b57c83-1b01-49db-8556-08a75dfaa83e',
                        '96122c28-0c48-41d4-8498-b6a361555832', 'd0a5f716-637a-4396-a778-b2f9f8eea0fa',
                        '63d0fd9a-226b-4fe4-bc4b-3a7f340fa008', '0de90f03-2e49-44f7-a008-594d01a489e8',
                        '1c5200e0-75e6-4b48-96c8-e88580a38149', '9b6eed07-7723-4cd7-b0d3-8f6adf163b6d',
                        '834c171b-3fde-4251-b2fc-69ba28e25b03', 'e256d72f-3631-46c4-b4a2-328d3585c9a4',
                        'e5e9f763-3dac-48c0-a2bf-c71b292f35d8', '4f1e6b77-8c8d-49be-8b06-ccd1fd0851da',
                        '460c6643-8e31-4e04-9189-eb7e3de180ad', '155c5d33-194f-4d36-8148-03f77e4aaa7f',
                        '4e91666e-c53c-47e9-9d78-bcf7658c53d9', 'a8c11601-84a8-45bf-a298-2cca5c56674b',
                        '68842db6-5026-4a9d-bdb0-7d24bf6e1557', '133af476-e67f-478c-ba86-897f9080d121',
                        'a4560039-793a-4732-ae97-8c54fc0d45cc', 'e574b4be-6301-407c-82b4-e83d367bb007',
                        'ddce4fbb-e7be-4b4a-81a9-ab919a39f9a7', '3aec8c6e-5007-4121-9495-78040bf00846',
                        '56caf81d-ca30-4411-b7a4-c0a718a5988e', '763f2b00-dfa7-45ba-972f-0bf5f43c1631')
    and author_id = '177881'
    and description =
        'Fechado por não responder à novos pedidos. Seu restaurante permanecerá fechado por um período de tempo.'
    and start_at > '2020-03-28 00:00:00.000000';
    """
    select = 'psql -AXtq -d "service=merchant-ro" -c "' + query + '"'
    rows = os.popen(select).read()
    rows = [] if len(rows) <= 1 else rows
    if (len(rows) > 0):
        rows = rows.strip().split("\n")
    return rows

def remove_unavailability(store_id, unavailability_id):
    url = merchant_api_url + '/admin/' + store_id + '/unavailabilities/' + unavailability_id
    headers = dict()
    headers['system-id'] = 'script-bruno'
    headers['user-id'] = 'bruno.nogueira'
    requests.delete(url, headers=headers)

def run():
    results = retrieve_ids()
    for result in results:
        register = result.split("|")
        # logging.debug("merchant_id " + register[0])
        # logging.debug("indisponibilidade id " + register[1])
        remove_unavailability(register[0], register[1])



run()
