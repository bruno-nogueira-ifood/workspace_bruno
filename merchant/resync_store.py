# -*- coding: utf-8 -*-

import os
import sys
import logging
import requests #To see more about requests lib http://docs.python-requests.org/en/master/user/quickstart/

# Resync de Store Config of all restaurants on application

logging.basicConfig(format='%(asctime)s %(message)s', level=logging.DEBUG)

merchant_api_url=None

env = sys.argv[1] if len(sys.argv) > 1 else 'DEV'

def retrieve_ids(offset, limit):
    query = """
        select id
        from ifood_merchant.merchant_stores st
        where not exists(
                select
                from ifood_merchant.store_configs sc
                where sc.store_id = st.id
                    and upper(sc.key) IN ('TESTE', 'TEST', 'LOADTEST', 'LOAD_TEST')
                    and upper(sc.value) IN ('S', 'TRUE'))
                and st.enabled is true
        order by id
        limit %d
        offset %d;
    """ % (limit, offset)
    select = 'psql -AXtq -d "service=merchant-ro" -c "' + query + '"'
    rows = os.popen(select).read()
    rows = [] if len(rows) <= 1 else rows
    if (len(rows) > 0):
        rows = rows.strip().split("\n")
    return rows

def resync_store_configs(stores_ids):
    logging.debug('Resync stores to ' + str(len(stores_ids)) + ' restaurants')
    requests.post(merchant_api_url + '/admin/store-resync/', json=stores_ids)


def run():
    limit = 50000
    offset = 0
    stores_ids = retrieve_ids(offset, limit)
    while(len(stores_ids) >= 1):
        resync_store_configs(stores_ids)
        offset += limit
        limit += 1000
        stores_ids = retrieve_ids(offset, limit)
    logging.debug('End script')


if __name__ == '__main__':
    if env == 'DEV':
        merchant_api_url = 'http://sa-east-1-elb-ifood-service-merchant.aws-development.dc-ifood.com/merchant'
        run()

    elif env == 'PROD':
        merchant_api_url = 'http://us-east-1-elb-ifood-service-merchant.aws-prod-legacy.dc-ifood.com/merchant'
        run()
    else:
        logging.info("You have to choose e right env: PROD or DEV")