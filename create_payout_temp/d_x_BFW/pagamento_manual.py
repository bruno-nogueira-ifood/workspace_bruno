#!/usr/bin/env python
# -*- coding: utf-8 -*-

import csv
import sys
import logging
import json
from datetime import datetime

logging.basicConfig(format='%(asctime)s %(message)s', level=logging.DEBUG)

env = sys.argv[1] if len(sys.argv) > 1 else 'DEV' 

def read(file_name):
    file = open('pagamento_manual.sql', "w")
    with open(file_name, 'r') as csvfile:
        spamreader = csv.reader(csvfile, delimiter=',', quotechar='|')
        for row in spamreader:
            sql = "INSERT INTO payout_manual (frn_id, a_pagar, referencia, payout_date) VALUES (" + str(row[0]) + ", " + str(row[1]) + ", '', '" + str(row[3])+ "');\n"
            file.write(sql)
    file.close()


def run():
    all_json = read('pagamento_manual.csv')


if __name__ == '__main__':
    if env == 'DEV':
        run()

    elif env == 'PROD':
        run()
    else:
        logging.info("You have to choose e right env: PROD or DEV")