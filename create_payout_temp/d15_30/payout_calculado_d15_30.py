#!/usr/bin/env python
# -*- coding: utf-8 -*-

import csv
import sys
import logging
import json
from datetime import datetime

logging.basicConfig(format='%(asctime)s %(message)s', level=logging.DEBUG)

env = sys.argv[1] if len(sys.argv) > 1 else 'DEV' 

def read(file_name):
    file = open('payout_calculado_d15_30.sql', "w")
    with open(file_name, 'r') as csvfile:
        spamreader = csv.reader(csvfile, delimiter=',', quotechar='|')
        for row in spamreader:
            sql = "INSERT INTO payout_calculado (frn_id, bill_date, repasse_total, payout_calculado_teorico, diferenca) VALUES (" + str(row[0]) + ", '" + str(row[1]) + "'"
            if row[3] is not '':
                sql = sql + ", '" + str(row[2])+ "'"
            else:
                sql = sql + ", NULL"

            if row[3] is not '':
                sql = sql + ", '" + str(row[3])+ "'"
            else:
                sql = sql + ", NULL"

            sql = sql + ", " + str(row[4]) + ");\n"
            file.write(sql)
    file.close()


def run():
    all_json = read('payout_calculado_d15_30.csv')


if __name__ == '__main__':
    if env == 'DEV':
        run()

    elif env == 'PROD':
        run()
    else:
        logging.info("You have to choose e right env: PROD or DEV")